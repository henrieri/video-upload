## Setting up the project
Project is created using Laravel framework. 
https://laravel.com/docs/5.7/

#### Composer install

`composer install`

#### Run npm install

`npm install`

#### Run js build

`npm run prod`

#### Setup env. Setup DB and AWS config as in example

`cp .env.example .env`

#### Generate app key

`php artisan key:generate`

#### Run migrations

`php artisan migrate`


### Dependencies
Project uses https://github.com/mhor/php-mediainfo 
to gather meta data about files.
To install it either 

On linux:

`$ sudo apt-get install mediainfo`

On Mac:

`$ brew install mediainfo`
