<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('uploads/init', 'VideoUploadController@getSignedUrl');

Route::post('uploads/s3/multipart/create', 'VideoUploadController@createMultipart');
Route::post('uploads/s3/multipart/upload', 'VideoUploadController@uploadMultipart');
Route::post('uploads/s3/multipart/complete', 'VideoUploadController@completeMultipart');
Route::resource('files', 'FileController');
Route::put('files/{file}/meta', 'FileMetaController@update');
Route::get('uploads/s3/{file}', 'VideoUploadController@getDownloadableFileUrl');
