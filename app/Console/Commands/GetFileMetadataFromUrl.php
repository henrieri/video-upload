<?php

namespace App\Console\Commands;

use App\Models\File;
use App\Services\File\FileMetadataService;
use App\Services\File\FileService;
use App\Services\File\S3Service;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GetFileMetadataFromUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:get-meta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param S3Service $service
     * @param FileService $fileService
     * @return mixed
     */
    public function handle(S3Service $service, FileMetadataService $fileService)
    {

        $file = File::where('name', 'small.mp4')->first();

        $fileService->enrichFile($file);


    }
}
