<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 9.09.18
 * Time: 17:10
 */

namespace App\Enums;


use MabeEnum\Enum;

class FileTypes extends Enum
{

    const FILE = 'file';
    const DIRECTORY = 'directory';
}