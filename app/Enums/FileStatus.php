<?php
namespace App\Enums;

use MabeEnum\Enum;

class FileStatus extends Enum
{
    const BEFORE_CREATE = 'before-create';
    const UPLOAD_STARTED = 'upload-started';
    const UPLOAD_COMPLETED = 'upload-completed';
}
