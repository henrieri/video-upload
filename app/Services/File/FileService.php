<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 9.09.18
 * Time: 16:11
 */

namespace App\Services\File;


use App\Enums\FileTypes;
use App\Models\File;
use Illuminate\Http\Request;
use Mhor\MediaInfo\MediaInfo;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileService
{

    public function create(Array $data = []): File
    {
        return File::create($data);
    }

    public function retrieveFileByUploadId($uploadId)
    {
        $file = File::where('upload_id', $uploadId)->first();

        if (!$file) {
            throw new NotFoundHttpException();
        }

        return $file;
    }

    public function byParentId($parentId = null)
    {
        return File::with('meta')->where('parent_id', $parentId)->get();
    }

    public function createDirectory(Request $request)
    {
        $parentId = $request->has('parent_id') ? $request->input('parent_id') : null;

        return $this->create([
            'name' => '',
            'file_type' => FileTypes::DIRECTORY,
            'parent_id' => $parentId
        ]);
    }

    public function updateFile(File $file, Request $request)
    {

        if ($request->has('name')) {

            $name = $request->input('name');

            $name = $name === null ? '' : $name;

            $file->name = $name;
        }

        if ($request->has('parent_id')) {
            $file->parent_id = $request->input('parent_id');
        }

        $file->save();

        return $file;
    }
}
