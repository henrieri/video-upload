<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 16.09.18
 * Time: 11:31
 */

namespace App\Services\File;


use App\Models\File;
use Aws\S3\S3Client;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class S3Service
{

    private $storage;
    private $adapter;
    private $client;
    private const FILES_PREFIX = 'files/';

    public function __construct(FilesystemAdapter $storage)
    {


        $this->storage = $storage;
        $this->adapter = $this->storage->getAdapter();
        $this->client = $this->adapter->getClient();
    }

    private function getConfig(File $file, $config = [])
    {

        $adapter = $this->getAdapter();

        return array_merge([
            'Bucket' => $adapter->getBucket(),
            'Key' => $this->getFileKey($file),
        ], $config);
    }

    private function getAdapter(): AwsS3Adapter
    {
        return $this->adapter;
    }

    private function getClient(): S3Client
    {

        return $this->client;
    }

    public function getTemporaryUrl(File $file)
    {

        return $this->storage->temporaryUrl(
            $this->getFileKey($file), now()->addMinutes(5), [
                'ResponseContentDisposition' => 'filename="'.$file->name.'"',
                'ResponseContentType' => $file->content_type
            ]
        );
    }

    private function getFileKey(File $file): string {
        $adapter = $this->getAdapter();

        return $adapter->getPathPrefix() . self::FILES_PREFIX . $file->id;
    }

    public function completeMultipartUpload(File $file, string $uploadId) {

        $partsList = $this->getClient()->listParts(
            $this->getConfig($file, [
                'UploadId' => $uploadId
            ])
        );

        $this->getClient()->completeMultipartUpload($this->getConfig($file, [
            'UploadId' => $uploadId,
            'MultipartUpload' => [
                'Parts' => $partsList['Parts']
            ]
        ]));
    }

    public function uploadMultipart(File $file, Request $request)
    {

        $client = $this->getClient();

        $config = $this->getConfig($file, [
            'UploadId' => $request->input('uploadId'),
            'PartNumber' => $request->input('partNumber'),
            'ContentLength' => $request->input('contentLength'),
        ]);

        $command = $client->getCommand('UploadPart', $config);

        $request = $client->createPresignedRequest(
            $command, now()->addMinutes(60)
        );

        return $request;
    }

    public function createMultipartUpload(File $file): string
    {

        $multipartUpload = $this->getClient()->createMultipartUpload($this->getConfig($file,[
            'ContentType' => $file->content_type
        ]));

        $uploadId = $multipartUpload->get('UploadId');

        return $uploadId;
    }

    public function destroyFile(File $file)
    {
        $this->storage->delete($this->getFileKey($file));
    }
}
