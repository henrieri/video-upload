<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 16.09.18
 * Time: 12:16
 */

namespace App\Services\File;


use App\Models\File;
use Mhor\MediaInfo\MediaInfo;

class FileMetadataService
{

    private $service;

    public function __construct(S3Service $service)
    {
        $this->service = $service;
    }

    public function enrichFile(File $file)
    {

        if (!$file->relationLoaded('meta')) {
            $file->load('meta');
        }

        $url = $this->service->getTemporaryUrl($file);

        $mediaInfo = new MediaInfo();

        $mediaInfoContainer = $mediaInfo->getInfo($url);

        $jsonEncoded = json_encode($mediaInfoContainer);

        $metaToInclude = [
            'parsedMetaObject' => $jsonEncoded
        ];

        $this->setMeta($file, $metaToInclude);

        $flattenedMeta = $this->flattenMeta(json_decode($jsonEncoded, true));

        $this->setMeta($file, $flattenedMeta);
    }

    private function flattenMeta($mediaInfoContainer, $flattenedArray = [], $currentPrefix = '')
    {

        foreach ($mediaInfoContainer as $key => $value) {

            if (is_array($value)) {

                $flattenedArray = $this->flattenMeta($value, $flattenedArray, $currentPrefix . $key . '.');
            } else {
                $flattenedArray[$currentPrefix . $key] = $value;
            }

        }

        return $flattenedArray;
    }

    public function setMeta(File $file, array $metaToInclude)
    {

        foreach ($metaToInclude as $key => $value) {

            if (!$file->meta->where('key', $key)->first()) {

                $file->meta()->create([
                    'key' => $key,
                    'value' => $value
                ]);
            }
        }
    }

    public function updateMetadata(File $file, array $metaData)
    {

        $collectedMetadata = collect($metaData);

        if (!$file->relationLoaded('meta')) {
            $file->load('meta');
        }

        foreach($collectedMetadata as $meta) {

            $meta = (object) $meta;

            $existingMeta = null;

            if (property_exists($meta, 'id')) {
                $existingMeta = $file->meta->where('id', $meta->id)->first();
            }

            if (!$existingMeta) {

                $file->meta()->create([
                    'key' => $meta->key,
                    'value' => $meta->value
                ]);
            } else {

                if($existingMeta->value === $meta->value && $existingMeta->key === $meta->key) {
                    continue;
                }

                $existingMeta->key = $meta->key;
                $existingMeta->value = $meta->value;
                $existingMeta->save();
            }
        }

        foreach($file->meta as $meta) {

            if (!$collectedMetadata->where('id', $meta->id)->first()) {
                $meta->delete();
            }
        }
    }
}
