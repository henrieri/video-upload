<?php

namespace App\Services\CustomArtisanMake\Console;


use Illuminate\Support\Str;

class ModelMakeCommand extends \Illuminate\Foundation\Console\ModelMakeCommand
{

    public function handle()
    {
        parent::handle();

        $this->createModelTest();
    }

    protected function createModelTest() {
        $controller = Str::studly(class_basename($this->argument('name')));

        $this->call('make:test', [
            'name' => "Models\Test{$controller}Model",
            '--unit' => true,
        ]);
    }

    protected function createController() {
        parent::createController();

        $this->createControllerTest();
    }

    protected function createControllerTest() {


        $controller = Str::studly(class_basename($this->argument('name')));

        $this->call('make:test', [
            'name' => "Controllers\Test{$controller}Controller",
            '--unit' => true,
        ]);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Models';
    }

    protected function getStub()
    {
        if ($this->option('pivot')) {
            return __DIR__.'/stubs/pivot.model.stub';
        }

        return __DIR__.'/stubs/model.stub';
    }
}