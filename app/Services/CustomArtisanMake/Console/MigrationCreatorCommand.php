<?php
namespace App\Services\CustomArtisanMake\Console;

class MigrationCreator extends \Illuminate\Database\Migrations\MigrationCreator
{

    protected function getStub($table, $create)
    {
        if (is_null($table)) {
            return $this->files->get($this->stubPath().'/blank.stub');
        }

        $stub = $create ? 'create.stub' : 'update.stub';

        if ($create) {
            return $this->files->get(__DIR__.'/stubs/create-table.stub');
        }

        return $this->files->get($this->stubPath()."/{$stub}");
    }


}