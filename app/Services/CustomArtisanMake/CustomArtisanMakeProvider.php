<?php

namespace App\Services\CustomArtisanMake;

use App\Services\CustomArtisanMake\Console\MigrationCreator;
use App\Services\CustomArtisanMake\Console\ModelMakeCommand;
use App\Services\CustomArtisanMake\Console\TestMakeCommand;
use Illuminate\Support\ServiceProvider;

class CustomArtisanMakeProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                TestMakeCommand::class
            ]);
        }
    }

    public function register()
    {

        $this->app->singleton('command.model.make', function ($app) {
            return new ModelMakeCommand($app['files']);
        });

        $this->app->singleton('migration.creator', function ($app) {
            return new MigrationCreator($app['files']);
        });

    }

    public function provides()
    {
        return [
            'migration.creator',
            'command.model.make'
        ];
    }
}
