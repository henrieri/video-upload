<?php

namespace App\Providers;

use App\Services\File\FileService;
use App\Services\File\S3Service;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FileService::class, function () {
            return new FileService();
        });

        $this->app->singleton(S3Service::class, function() {
            return new S3Service(Storage::disk('s3'));
        });
    }
}
