<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Services\File\FileService;
use App\Services\File\S3Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, FileService $fileService)
    {

        $parentId = $request->has('parent_id') ? $request->input('parent_id') : null;

        return $this->filesResponse($fileService->byParentId($parentId));
    }

    public function store(Request $request, FileService $fileService)
    {
        $directory = $fileService->createDirectory($request);

        return $this->filesResponse($directory);
    }

    /**
     * Display the specified resource.if
     *
     * @param  \App\Models\File $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {

        $file->load('meta');

        return $this->filesResponse($file);
    }

    public function update(File $file, Request $request, FileService $fileService)
    {
        $file = $fileService->updateFile($file, $request);

        return $this->filesResponse($file);
    }

    private function filesResponse($files) {

        if ($files instanceof File) {
            $files = [ $files ];
        }

        return response()->json(['files' => $files]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File $file
     * @param S3Service $s3Service
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(File $file, S3Service $s3Service)
    {
        $s3Service->destroyFile($file);

        $file->delete();

        return response($file->id, 204);
    }
}
