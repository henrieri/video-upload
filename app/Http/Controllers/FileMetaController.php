<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\FileMeta;
use App\Services\File\FileMetadataService;
use Illuminate\Http\Request;

class FileMetaController extends Controller
{

    /**
     * @param Request $request
     * @param File $file
     * @param FileMetadataService $service
     */
    public function update(Request $request, File $file, FileMetadataService $service)
    {
        $service->updateMetadata($file, $request->input('meta'));

        $file->load('meta');

        return response()->json([
            'files' => [ $file ]
        ]);
    }


}
