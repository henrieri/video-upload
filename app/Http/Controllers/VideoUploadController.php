<?php

namespace App\Http\Controllers;

use App\Enums\FileStatus;
use App\Models\File;
use App\Models\FileMeta;
use App\Services\File\FileMetadataService;
use App\Services\File\FileService;
use App\Services\File\S3Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class VideoUploadController extends Controller
{

    private $service;

    public function __construct(S3Service $s3Service)
    {
        $this->service = $s3Service;
    }

    public function createMultipart(Request $request, FileService $fileService)
    {

        $request->validate([
            'filename' => 'required'
        ]);

        $filename = $request->input('filename');

        $file = $fileService->create([
            'name' => $filename,
            'status' => FileStatus::BEFORE_CREATE,
            'content_type' => $request->input('contentType'),
            'parent_id' => $request->has('parent_id') ? $request->input('parent_id') : null
        ]);

        $uploadId = $this->service->createMultipartUpload($file);

        $file->update([
            'upload_id' => $uploadId,
            'status' => FileStatus::UPLOAD_STARTED
        ]);

        return response()->json([
            'uploadId' => $uploadId
        ]);
    }

    public function uploadMultipart(Request $request, FileService $fileService)
    {

        $file = $fileService->retrieveFileByUploadId($request->input('uploadId'));

        $request = $this->service->uploadMultipart($file, $request);

        return $this->signedUriResponse($request);
    }

    public function completeMultipart(Request $request, FileService $fileService, FileMetadataService $fileMetadataService)
    {

        $file = $fileService->retrieveFileByUploadId($request->input('uploadId'));

        $this->service->completeMultipartUpload($file, $request->input('uploadId'));

        $file->update([
            'status' => FileStatus::UPLOAD_COMPLETED
        ]);

        $fileMetadataService->enrichFile($file);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function signedUriResponse($request)
    {
        return response()->json([
            'signedUrl' => (string)$request->getUri()
        ]);
    }


    public function getDownloadableFileUrl(File $file)
    {

        $url = $this->service->getTemporaryUrl($file);

        return response()->json([
            'signedUrl' => $url
        ]);
    }


    public function editFileMetadata()
    {

    }

    public function editFileTags()
    {

    }
}
