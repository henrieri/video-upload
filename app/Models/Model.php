<?php

namespace App\Models;


use App\Traits\UuidTrait;

class Model extends \Illuminate\Database\Eloquent\Model
{
    use UuidTrait;

    public $incrementing = false;

    protected $guarded = [];
}