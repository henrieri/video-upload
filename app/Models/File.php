<?php

namespace App\Models;

use App\Models\Model as BaseModel;

/**
 * @property mixed meta
 */
class File extends BaseModel
{
    public function meta()
    {
        return $this->hasMany(FileMeta::class);
    }
}
