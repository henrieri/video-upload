<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')->on('files')
                ->onDelete('set null');
            $table->string('file_type')->default(\App\Enums\FileTypes::FILE);
            $table->string('name')->default('Untitled');
            $table->string('content_type')->nullable();
            $table->string('status')->nullable();
            $table->string('upload_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
