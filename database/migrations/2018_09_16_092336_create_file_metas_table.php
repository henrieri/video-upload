<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_metas', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('file_id');
            $table->foreign('file_id')
                ->references('id')->on('files')
                ->onDelete('cascade');

            $table->string('key');
            $table->text('value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_metas');
    }
}
