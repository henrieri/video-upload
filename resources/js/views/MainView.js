import React, { Component } from 'react'
import Directory from 'components/Directory'
import VideoUploader from 'components/VideoUploader'

class MainView extends Component {
  render () {
    return (
      <div className='main-view'>
        <Directory /><VideoUploader />
      </div>
    )
  }
}

export default MainView
