import PrimaryButton from 'components/Buttons/PrimaryButton'
import SecondaryButton from 'components/Buttons/SecondaryButton'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import DangerButton from 'components/Buttons/DangerButton'
import { fetchSingleFile, getFiles, saveFileMeta } from 'store/reducers/files'
import './SingleFileView.scss'

class SingleFileView extends Component {
  constructor () {
    super()

    this.state = {
      meta: [],
      isLoaded: false
    }
  }

  async componentDidMount () {
    await this.props.dispatch(fetchSingleFile(this.getFileId()))

    this.setState({
      meta: this.getFile().meta,
      isLoaded: true
    })
  }

  getFileId() {
    return this.props.match.params.file;
  }

  getFile () {
    return this.props.files.find(file => file.id === this.getFileId())
  }

  handleMetaChange = (evt, meta, metaProp) => {
    const metaIndex = this.state.meta.findIndex(m => m.id === meta.id)

    const newMetaArray = [ ...this.state.meta ]

    newMetaArray[metaIndex] = {
      ...newMetaArray[metaIndex],
      [metaProp]: evt.target.value
    }

    this.setState({
      meta: newMetaArray
    })
  }

  addRow = () => {
    this.setState({
      meta: [
        {},
        ...this.state.meta
      ]
    })
  }

  hasDataChanged () {
    return JSON.stringify(this.state.meta) !== JSON.stringify(this.getFile().meta)
  }

  deleteMeta (argMeta) {
    const newMetaArray = this.state.meta.filter(meta => meta.id !== argMeta.id);

    this.setState({
      meta: newMetaArray
    })
  }

  saveData = async () => {
    this.setState({
      isLoaded: false
    });

    await this.props.dispatch(saveFileMeta(this.getFileId(), this.state.meta))

    this.setState({
      meta: this.getFile().meta,
      isLoaded: true
    })
  }

  handleReset = () => {
    this.setState({
      meta: this.getFile().meta
    })
  }

  render () {
    if (!this.state.isLoaded) {
      return (<div>Loading ...</div>)
    }

    const metaRows = this.state.meta.map((meta) => (
      <div className='meta-row' key={meta.id}>
        <div className='meta-key'><input onChange={(evt) => this.handleMetaChange(evt, meta, 'key')} type='text'
          name={`${meta.id}[key]`} value={meta.key} /></div>
        <div className='meta-value'><input onChange={(evt) => this.handleMetaChange(evt, meta, 'value')} type='text'
          name={`${meta.id}[value]`} value={meta.value} /></div>
        <DangerButton onClick={() => this.deleteMeta(meta)}>Delete</DangerButton>
      </div>
    ))

    return (
      <div className='single-file-view file-meta-data'>
        <div className="back-btn-container">
          <SecondaryButton to='/'>Go back</SecondaryButton>
        </div>
        <h1>Meta</h1>
        <div className='box'>
          <div className='action-container'>
            <PrimaryButton onClick={this.addRow}>Add row</PrimaryButton>
            { this.hasDataChanged() ? (<PrimaryButton onClick={this.saveData}>Save data</PrimaryButton>) : ''}
            { this.hasDataChanged() ? (<SecondaryButton onClick={this.handleReset}>Reset</SecondaryButton>) : ''}
          </div>
          <div>
            {metaRows}
          </div>
        </div>
      </div>
    )
  }
}

SingleFileView.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.object,
  files: PropTypes.array
}

SingleFileView.defaultProps = {}

const mapStateToProps = state => ({
  files: getFiles(state)
})

export default connect(mapStateToProps)(SingleFileView)
