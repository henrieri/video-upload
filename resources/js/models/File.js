import Model from 'models/Model'

export const FileTypes = {
  FILE: 'file',
  DIRECTORY: 'directory'
}

export default class File extends Model {

  isDirectory() {
    return FileTypes.DIRECTORY === this.file_type;
  }

  isFile() {
    return FileTypes.FILE === this.file_type;
  }

  belongsToDir(directoryId) {
    return this.parent_id === directoryId;
  }
}
