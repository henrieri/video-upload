import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from 'App'
import 'app.scss'
import 'bootstrap'
import configureStore from 'store'
import { BrowserRouter } from 'react-router-dom'
import 'core/icons';

const store = configureStore()

render(<BrowserRouter>
  <Provider store={store}><App /></Provider>
</BrowserRouter>, document.getElementById('app'))

if (module.hot && process.env.NODE_ENV === 'development') {
  module.hot.accept()
}
