import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Fileparts extends Component {
  render () {
    const fileParts = this.props.fileParts.map(filePart => (
      <div className='file-part' key={filePart.number}>
        <div className='column'>
          {filePart.number}
        </div>
        <div className='column'>
          {filePart.loaded}
        </div>
        <div>
          {filePart.total}
        </div>
      </div>
    ))

    return (
      <div className='file-parts-container box'>
        <h2>File parts being uploaded</h2>
        {fileParts}
      </div>
    )
  }
}

Fileparts.propTypes = {
  fileParts: PropTypes.array
}

export default Fileparts
