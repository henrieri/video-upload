import PrimaryButton from 'components/Buttons/PrimaryButton'
import React, { Component } from 'react'
import Filerow from 'components/Filerow'
import './Directory.scss'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { createDirectory, fetchFiles, fetchSingleFile, getFiles } from 'store/reducers/files'
import { sortBy } from 'lodash'
import File, { FileTypes } from 'models/File'

class Directory extends Component {

  constructor () {
    super()

    this.state = {
      editName: null
    }
  }

  getDirectoryId () {
    return this.props.match.params.file !== undefined ? this.props.match.params.file : null
  }

  getDirectory () {
    return this.props.files.find(directory => directory.id === this.getDirectoryId())
  }

  componentDidMount () {
    this.fetchDirectoryFiles();
  }

  fetchDirectoryFiles() {
    const directoryId = this.getDirectoryId()

    this.props.dispatch(fetchFiles(directoryId))

    if (directoryId !== null) {
      this.props.dispatch(fetchSingleFile(directoryId))
    }
  }

  componentDidUpdate(oldProps) {
    if (oldProps.match.params.file === this.props.match.params.file) {
      return;
    }

    this.fetchDirectoryFiles();
  }

  handleEditStop = () => {
    this.setState({
      editName: null
    })
  }

  handleEditStart = (id) => {
    this.setState({
      editName: id
    })
  }

  getParentDirectory () {
    return new File({ id: this.getDirectory().parent_id, name: '..', file_type: FileTypes.DIRECTORY })
  }

  createDirectory = async () => {
    await this.props.dispatch(createDirectory(this.getDirectoryId()))

    const filesCopy = [...this.props.files]
    filesCopy.sort((a, b) => a.created_at > b.created_at ? -1 : 1)

    this.setState({
      editName: filesCopy[0].id
    })
  }

  getFiles () {
    const files = this.props.files.filter(file => file.belongsToDir(this.getDirectoryId()))

    return sortBy(files, (file) => file.isDirectory() ? 0 : 1)
  }

  render () {
    const state = this.state

    const fileRows = this.getFiles().map((file) => (
      <Filerow handleEditStart={this.handleEditStart} handleEditStop={this.handleEditStop} file={file} key={file.id}
               editName={file.id === state.editName} />))

    return (
      <div className='directory box'>
        <div className="action-header">
          <PrimaryButton onClick={this.createDirectory}>Create Folder</PrimaryButton>
        </div>
        <div className='file-headers'>
          <div className='column filename'>
            Your files
          </div>
        </div>
        {this.getDirectory() ? (<Filerow isParentDirectory file={this.getParentDirectory()} />) : ''}
        {fileRows}
      </div>
    )
  }
}

Directory.propTypes = {
  files: PropTypes.array,
  dispatch: PropTypes.func,
  match: PropTypes.object,
}
Directory.defaultProps = {}

const mapStateToProps = state => ({
  files: getFiles(state)
})

export default DragDropContext(HTML5Backend)(withRouter(connect(mapStateToProps)(Directory)))
