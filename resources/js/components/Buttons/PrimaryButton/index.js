import BaseButton from 'components/Buttons/BaseButton'
import './PrimaryButton.scss';

export default class PrimaryButton extends BaseButton {
  className = 'primary-button'
}
