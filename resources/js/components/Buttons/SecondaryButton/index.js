import BaseButton from 'components/Buttons/BaseButton'
import './SecondaryButton.scss';

export default class SecondaryButton extends BaseButton {
  className = 'secondary-button'
}
