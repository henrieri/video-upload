import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './BaseButton.scss'
import { Link } from 'react-router-dom'

class BaseButton extends Component {

  className = 'base-button'

  buttonTag = (props) => {
    if (this.props.to !== undefined) {

      return (<Link {...props}>{this.props.children}</Link>)
    }

    return (<button {...props}>{this.props.children}</button>)
  }

  render () {
    return (
      <this.buttonTag to={this.props.to} className={`base-button ${this.className}`} onClick={this.props.onClick}>
        {this.props.children}
      </this.buttonTag>
    )
  }
}

BaseButton.propTypes = {
  children: PropTypes.any,
  to: PropTypes.any
}
BaseButton.defaultProps = {}

export default BaseButton
