import BaseButton from 'components/Buttons/BaseButton'
import './DangerButton.scss';

export default class DangerButton extends BaseButton {
  className = 'danger-button'
}
