import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { fetchFiles } from 'store/reducers/files'
import PropTypes from 'prop-types'
import S3Service from 'core/S3Service'
import './VideoUploader.scss'
import Fileparts from 'components/Fileparts'

const UploadingStates = {
  IDLE: 'Idle',
  STARTING: 'Starting...',
  UPLOADING: 'Uploading...',
  ANALYZING: 'Analyzing...',
  COMPLETED: 'Completed'
}

class VideoUploader extends Component {
  constructor () {
    super()

    this.state = {
      progress: 0,
      speed: 0,
      loaded: 0,
      total: 0,
      fileParts: [],
      uploadingState: UploadingStates.IDLE
    }
  }

  handleProgress = (progressEvent) => {

    this.setState({
      uploadingState: UploadingStates.UPLOADING
    })

    const { loaded, total, fileParts } = progressEvent

    const percentCompleted = Math.floor((loaded * 100) / total * 100) / 100

    const timeNow = Date.now()

    const timeUploaded = timeNow - this.uploadStart // ms

    const speed = Math.round(loaded / timeUploaded * 1000)

    this.setState({
      progress: percentCompleted,
      speed,
      loaded,
      total,
      fileParts
    })

    if (percentCompleted >= 100) {
      this.setState({
        uploadingState: UploadingStates.ANALYZING
      })
    }
  }

  expectedTimeLeft () {
    const leftToDownload = this.state.total - this.state.loaded

    if (this.state.speed === 0) {
      return 'Unknown'
    }

    return Math.round(leftToDownload / this.state.speed)
  }

  formatBytes (bytes) {
    let unitArray = ['b', 'kB', 'MB', 'GB', 'TB']

    let index = 0

    while (bytes > 100) {
      bytes = bytes / 1000
      index++
    }

    return (Math.round(bytes * 10) / 10).toFixed(1) + unitArray[index]
  }

  handleDrop = async (acceptedFiles) => {
    const file = acceptedFiles[0]

    this.uploadStart = Date.now()

    this.setState({
      uploadingState: UploadingStates.STARTING
    })

    const service = new S3Service(
      {
        progressHandler: this.handleProgress,
        completeCallback: () => {
          this.setState({
            uploadingState: UploadingStates.COMPLETED
          })

          this.props.dispatch(fetchFiles(directoryId))
        }
      }
    )

    const directoryId = this.props.match.params.file ? this.props.match.params.file : null;

    await service.uploadFile(file, directoryId)


  }

  render () {
    return (
      <div className='video-uploader'>
        <div className='video-upload-statistics box'>
          <div className='stat-row'>
            <div className='stat-key'>
              Status
            </div>
            <div className='stat-value'>
              {this.state.uploadingState}
            </div>
          </div>
          <div className='stat-row'>
            <div className='stat-key'>
              Progress
            </div>
            <div className='stat-value'>
              {this.state.progress}%
            </div>
          </div>
          <div className='stat-row'>
            <div className='stat-key'>
              Average speed
            </div>
            <div className='stat-value'>
              {this.formatBytes(this.state.speed)}/s
            </div>
          </div>
          <div className='stat-row'>
            <div className='stat-key'>
              Uploaded
            </div>
            <div className='stat-value'>
              {this.formatBytes(this.state.loaded)}
            </div>
          </div>
          <div className='stat-row'>
            <div className='stat-key'>
              Total size
            </div>
            <div className='stat-value'>
              {this.formatBytes(this.state.total)}
            </div>
          </div>
          <div className='stat-row'>
            <div className='stat-key'>
              Expected duration left
            </div>
            <div>
              {this.expectedTimeLeft()}s
            </div>
          </div>
        </div>
        { [UploadingStates.IDLE, UploadingStates.COMPLETED].includes(this.state.uploadingState) ?
          (<Dropzone
            accept='video/*'
            className='dropzone box'
            onDrop={this.handleDrop}>
            Click here or drop a file to upload
          </Dropzone>) : ''}

        <Fileparts fileParts={this.state.fileParts} />
      </div>
    )
  }
}

VideoUploader.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.object
}
VideoUploader.defaultProps = {}

export default withRouter(connect(() => ({}))(VideoUploader))
