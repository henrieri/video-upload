import DangerButton from 'components/Buttons/DangerButton'
import SecondaryButton from 'components/Buttons/SecondaryButton'
import { ItemTypes } from 'components/Filerow/Constants'
import Api from 'core/Api'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Filerow.scss'
import { DragSource, DropTarget } from 'react-dnd'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { deleteFile, updateFile } from 'store/reducers/files'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Filerow extends Component {

  constructor (props) {
    super(props)

    this.state = {
      form: {
        ...props.file,
      },
      editingName: false
    }
  }

  componentDidMount () {
    window.addEventListener('keypress', this.handleKeyPress)
    window.addEventListener('click', this.handleClick)
  }

  componentWillUnmount () {
    window.removeEventListener('keypress', this.handleKeyPress)
    window.removeEventListener('click', this.handleClick)
  }
  handleClick = (e) => {
    if (!this.props.editName) {
      return;
    }

    if (!this.node.contains(e.target)) {
      this.updateName();
    }
  }

  handleKeyPress = (e) => {
    if (e.code === 'Enter') {
      this.updateName();
    }
  }

  updateDirectory = async (directoryId) => {
    await this.props.dispatch(updateFile(this.props.file.id, { parent_id: directoryId }))
  }

  updateName = async () => {

    if (!this.props.editName) {
      return;
    }

    await this.props.dispatch(updateFile(this.props.file.id, this.state.form))
    this.props.handleEditStop();
  }

  handleDownloadClick = () => {

    Api.get(`uploads/s3/${this.props.file.id}`).then(response => {
      window.location = response.signedUrl
    })
  }

  handleFormChange (evt, property) {
    this.setState({
      form: {
        ...this.form,
        [property]: evt.target.value
      }
    })
  }

  isEditingName () {
    return this.props.editName
  }

  handleEditName = () => {
    this.props.handleEditStart(this.props.file.id);
  }

  deleteFile = (fileId) => {
    this.props.dispatch(deleteFile(fileId))
  }

  renderNameEditor = () => (
    <div className='column'><input autoFocus type="text" value={this.state.form.name}
                                   onChange={(evt) => this.handleFormChange(evt, 'name')} /></div>
  )
  renderFileName = () => (
    <div className='column filename' onClick={this.handleDownloadClick} onFocus={(e) => e.target.select()}>{this.props.file.name}</div>)

  renderDirectoryName = () => (<div className='column'><Link to={`/directory/${this.props.file.id}`}>{this.props.file.name}</Link></div>)

  renderName = () => this.props.file.isFile() ? this.renderFileName() : this.renderDirectoryName()

  render () {
    const { file, isDragging, connectDragSource, connectDropTarget } = this.props
    const { id } = file;

    if (this.props.isParentDirectory) {
      return connectDropTarget(
        <div className='filerow'>
          <div className="column"><FontAwesomeIcon icon='folder'/></div>
          <Link to={id ? `/directory/${id}` : '/' } className="column">
            ..
          </Link>
        </div>
      )
    }

    return connectDropTarget(connectDragSource(
      <div style={{ opacity: isDragging ? 0.5 : 1 }} className='filerow' ref={node => { this.node = node }}>
        <div className="column"><FontAwesomeIcon icon={file.isDirectory() ? 'folder' : 'video' }/></div>
        {this.isEditingName() ? this.renderNameEditor() : this.renderName()}
        <div className='column actions pull-right'>
          {file.isFile() ? (<SecondaryButton to={`/files/${file.id}`}><FontAwesomeIcon icon='info' /></SecondaryButton>) : ''}
          <SecondaryButton onClick={this.handleEditName}><FontAwesomeIcon icon='edit' /></SecondaryButton>
          <DangerButton onClick={() => this.deleteFile(file.id)}><FontAwesomeIcon icon='trash' /></DangerButton>
        </div>
      </div>
    ))
  }
}

Filerow.propTypes = {
  file: PropTypes.object,
  editName: PropTypes.bool,
  dispatch: PropTypes.func,
  isParentDirectory: PropTypes.bool,
  handleEditStop: PropTypes.func,
  handleEditStart: PropTypes.func,
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  connectDropTarget: PropTypes.func.isRequired
}
Filerow.defaultProps = {
  isParentDirectory: false
}

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

function targetCollect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

const filerowSource = {
  beginDrag(props, monitor, component) {
    console.log(component);
    return {
      file: props.file,
      updateDirectory: component.updateDirectory
    }
  }
}

const directoryTarget = {
  canDrop(props, monitor) {
    console.log(props);
    if (monitor.getItem().file.id === props.file.id) {
      return false;
    }
    console.log('can drop', props.file.isDirectory())
    return props.file.isDirectory();
  },
  drop(props, monitor) {
    console.log(props);
    const item = monitor.getItem();
    item.updateDirectory(props.file.id);
  },

};

const dragAndDrop = (component) => {
  return DropTarget([ItemTypes.FILEROW], directoryTarget, targetCollect)(component);
}

export default dragAndDrop(withRouter(connect()(DragSource(ItemTypes.FILEROW, filerowSource, collect)(Filerow))))

