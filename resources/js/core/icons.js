import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faEdit, faInfo, faFolder, faVideo } from '@fortawesome/free-solid-svg-icons'
library.add(faTrash)
library.add(faEdit)
library.add(faInfo)
library.add(faFolder)
library.add(faVideo)
