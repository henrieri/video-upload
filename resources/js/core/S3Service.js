import Api from './Api'
import axios from 'axios'

const defaultConfig = {
  partSize: 50 * 1024 * 1024,
  completeCallback: () => { console.log('Upload completed') }
}

class Filepart {
  constructor (contents, number) {
    this.signedUrl = null
    this.contents = contents
    this.progress = 0
    this.number = number
  }

  setProgressHandler (progressHandler) {
    this.progressHandler = progressHandler
  }

  setSignedUrl (signedUrl) {
    this.signedUrl = signedUrl
  }

  upload () {
    return axios.put(this.signedUrl, this.contents, {
      onUploadProgress: this.handleProgress
    })
  }

  handleProgress = (progressEvent) => {
    this.loaded = progressEvent.loaded
    this.total = progressEvent.total
    this.progressHandler(progressEvent)
  }

  getNumber () {
    return this.number
  }

  getContentLength () {
    return this.contents.size
  }
}

export default class S3Service {
  constructor (config) {
    this.config = {
      ...defaultConfig,
      ...config
    }
  }

  uploadFile (file, directoryId) {
    this.file = file
    this.fileParts = []
    this.promises = []
    this.start(directoryId)
  }

  async start (directoryId) {
    await this.createMultipartUpload(directoryId)

    let currentFileIndex = 0
    let contentUploadStart = 0
    let contentUploadEnd = 0

    while (contentUploadEnd < this.file.size) {
      contentUploadStart = this.config.partSize * currentFileIndex++
      contentUploadEnd = Math.min(contentUploadStart + this.config.partSize, this.file.size)
      this.fileParts.push(new Filepart(this.file.slice(contentUploadStart, contentUploadEnd), currentFileIndex))
    }

    const promises = []

    this.fileParts.forEach((part) => {
      promises.push(
        this.signUploadPart(part)
      )
    })

    await Promise.all(promises)

    await this.sendAllParts().then(() => {
      this.completeMultipartUpload()
    })
  }

  async sendAllParts () {
    for (const part of this.fileParts) {
      part.setProgressHandler(this.handleProgress)
      await part.upload()
    }
  }

  handleProgress = () => {
    let total = 0
    let loaded = 0

    this.fileParts.forEach((filePart) => {
      total += filePart.getContentLength()
      loaded += filePart.loaded || 0
    })

    this.config.progressHandler({
      loaded,
      total,
      fileParts: this.fileParts
    })
  }

  sendCommand (command, params = {}) {
    return Api.post(`uploads/s3/multipart/${command}`, params)
  }

  signUploadPart (part) {
    return this.sendCommand('upload', {
      partNumber: part.getNumber(),
      contentLength: part.getContentLength(),
      uploadId: this.uploadId
    }).then((response) => {
      part.setSignedUrl(response.signedUrl)
    })
  }

  createMultipartUpload (directoryId) {
    return this.sendCommand('create', {
      filename: this.file.name,
      contentType: this.file.type,
      parent_id: directoryId
    }).then(response => {
      this.uploadId = response.uploadId
    })
  }

  completeMultipartUpload () {
    return this.sendCommand('complete', {
      uploadId: this.uploadId
    }).then( () => {
      this.config.completeCallback()
    })
  }
}
