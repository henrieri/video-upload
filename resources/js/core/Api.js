import axios from 'axios'

export default class Api {
  static request (method, endpoint, data = {}, params = {}) {
    const finalEndpoint = `/api/${endpoint}/`

    return axios.request(finalEndpoint, {
      method,
      data,
      params
    }).then(response => response.data)
  }

  static put (endpoint, data) {
    return this.request('put', endpoint, data)
  }

  static post (endpoint, data = {}) {
    return this.request('post', endpoint, data)
  }

  static get (endpoint, query = {}) {

    return this.request('get', endpoint, {}, query)
  }

  static delete (endpoint, data = {}) {
    return this.request('delete', endpoint, data)
  }
}
