import Api from 'core/Api'
import File from 'models/File'
const ADD_FILES = 'files/ADD_FILES'
const DELETE_FILE = 'files/DELETE_FILE'

const initialState = {
  entities: {},
  byId: []
}

export default function files (state = initialState, action) {
  switch (action.type) {
    case ADD_FILES:
      const byId = [...state.byId]
      const newEntities = action.files.reduce((entities, file) => {
        if (!byId.includes(file.id)) {
          byId.push(file.id)
        }

        return {
          ...entities,
          [file.id]: file
        }
      }, {})

      return {
        entities: {
          ...state.entities,
          ...newEntities
        },
        byId
      }
    case DELETE_FILE:
      return _deleteFile(state, action)

    default:
      return state
  }
}

export const _deleteFile = (state, action) => {
  const { [action.fileId]: remove, ...newEntities } = state.entities;
  const newById = state.byId.filter(id => id !== action.fileId);

  return {
    entities: newEntities,
    byId: newById
  }
}

export const addFiles = files => ({ type: ADD_FILES, files })

export const removeFile = fileId => ({ type: DELETE_FILE, fileId })

export const fetchFiles = (directoryId) =>
  (dispatch) => Api.get('files', { parent_id: directoryId }).then(response => dispatch(addFiles(response.files)))

export const fetchSingleFile = (fileId) =>
  (dispatch) => Api.get(`files/${fileId}`).then(response => {
    return dispatch(addFiles(response.files))
  })

export const deleteFile = (fileId) =>
  (dispatch) => Api.delete(`files/${fileId}`).then(() => {
    return dispatch(removeFile(fileId))
  })

export const updateFile = (fileId, data) =>
  (dispatch) => Api.put(`files/${fileId}`, data).then((response) => {
    return dispatch(addFiles(response.files))
  })

export const createDirectory = (directoryId) => (dispatch) => Api.post(`files`, {
  parent_id: directoryId
}).then((response) => {
  return dispatch(addFiles(response.files.map( file => ({ ...file, nameCreated: false }))))
});

export const saveFileMeta = (fileId, meta) => (dispatch) => Api.put(`files/${fileId}/meta`, { meta })
  .then(response => dispatch(addFiles(response.files)))

export const getFiles = state => state.files.byId.map(id => state.files.entities[id]).map(file => new File(file));
