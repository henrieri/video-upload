import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import reducer from 'store/reducers'

export default function configureStore () {

  const devToolsExtension = (store) => {
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      return window.__REDUX_DEVTOOLS_EXTENSION__()(store)
    }
    return store
  }

  const store = createStore(reducer, compose(applyMiddleware(thunk), devToolsExtension))

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('store/reducers', () => {
      const nextReducer = require('store/reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
