import { combineReducers } from 'redux'
import files from 'store/reducers/files'

const rootReducer = combineReducers({
  files
})

export default rootReducer
