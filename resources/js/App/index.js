import React, { Component } from 'react'
import SingleFileView from 'views/SingleFileView/index'
import MainView from 'views/MainView'
import './App.scss'
import { Route, Switch } from 'react-router'

export default class App extends Component {
  render () {
    return (<div className='app'>
      <Switch>
        <Route exact path='/' component={MainView} />
        <Route path={`/files/:file`} component={SingleFileView} />
        <Route path={`/directory/:file`} component={MainView} />
      </Switch>
    </div>)
  }
}
